#!/user/bin/env groovy

def call() {
    echo 'build the docker image'
    withCredentials([usernamePassword(credentials: 'docker-hub', passwordVariable: 'PASS', usernameVariable: 'USER')]){
    sh 'docker build -t fpetesting/demo-app:2.0 .'
    sh 'echo $PASS | docker login -u $USER --password-stdin'
    sh 'docker push fpetesting/demo-app:2.0'
    }
}
